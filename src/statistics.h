/*****************************************************************************/
/* statistics.h 								     */
/*****************************************************************************/

#ifndef	__STATISTICS_H_INCLUDED
#define	__STATISTICS_H_INCLUDED 1

/*****************************************************************************/

/* median():
   Sorts the array 'data' of 'n' real values into ascending order and returns
   the median of the dataset.						     */
double	median(double *data,int n);

/* mean():
   Mean of the array 'data'.						     */
double	mean(double *data,int n);

/*****************************************************************************/

#endif

/*****************************************************************************/
                                
